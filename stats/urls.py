from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^teams/(?P<team_id>\d+)/(?P<team_slug>.+)/roster/$', views.teamRoster, name="team-roster"),
    url(r'^teams/(?P<team_id>\d+)/(?P<team_slug>.+)/schedule/$', views.teamSchedule, name="team-schedule"),
    url(r'^teams/(?P<team_id>\d+)/(?P<team_slug>.+)/$', views.teamPage, name="team-page"),
    url(r'^teams/$', views.teamIndex, name="team-index"),
    url(r'^league/(?P<league_id>\d+)/(?P<league_slug>.+)/$', views.leaguePage, name="league-page"),
    url(r'^cups-and-leagues/(?P<cup_id>\d+)/(?P<cup_name>.+)/$', views.cupPage, name="cup-page"),
    url(r'^cups-and-leagues/$', views.clIndex, name="cups-and-leagues"),
    url(r'^players/$', views.playerIndex, name="players"),
    url(r'^players/(?P<player_id>\d+)/(?P<player_slug>.+)/$', views.playerPage, name="player-page"),
    url(r'^match/(?P<match_id>\d+)/(?P<match_slug>.+)/$', views.matchPage, name="match"),
]
