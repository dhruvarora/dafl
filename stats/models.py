from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField
from ckeditor_uploader.fields import RichTextUploadingField

FORMAT_CHOICES = (
    (1, "League"),
    (2, "League + Knockout"),
    (3, "Group-Wise League + Knockout"),
    (4, "Group-Wise League + Leauge"),
    (5, "Group Wise League + Group Wise League + Knockouts"),
    (6, "League + Group-wise Leauge"),
    (7, "Group-wise League + League + Knockout"),
)
PLAYER_TYPES = (
    (1, "Centre-back"),
    (2, "Sweeper"),
    (3, "Full-back"),
    (4, "Wing-back"),
    (5, "Centre midfield"),
    (6, "Defensive midfield"),
    (7, "Attacking midfield"),
    (8, "Wide midfield"),
    (9, "Centre forward"),
    (10, "Second striker"),
    (11, "Winger"),
    (12, "Goalkeeper"),
    (13, "Defensive"),
    (14, "Midfield"),
    (15, "Attacking"),
)

MANAGER_TYPES = (
    (1, "Technical Director"),
    (2, "Head Coach"),
    (3, "Asst. Coach"),
    (4, "GK Coach"),
    (5, "Manager"),
)

class Competition(models.Model):
    date_added = models.DateField(default=timezone.now)
    name = models.CharField(max_length=150)
    league_format = models.IntegerField(choices=FORMAT_CHOICES, default=0)
    logo = models.FileField(upload_to="competitions/logos/", max_length=150)

    def __str__(self):
        return self.name

class Season(models.Model):
    date_added = models.DateField(default=timezone.now)
    name = models.CharField(max_length=50)
    competiton = models.ForeignKey(Competition)

    def __str__(self):
        return self.name

class Venue(models.Model):
    date_added = models.DateField(default=timezone.now)
    name = models.CharField(max_length=150)
    address = models.TextField(blank=True, null=True)
    city = models.CharField(blank=True, null=True, max_length=50)
    def __str__(self):
        return self.name

class Player(models.Model):
    date_added = models.DateField(default=timezone.now)
    name = models.CharField(max_length=150)
    picture = models.FileField(max_length=150, upload_to="players", blank=True, null=True)
    dob = models.DateField(blank=True, null=True, help_text="Date of Birth")
    nationality = models.CharField(max_length=50, default="Indian")
    height = models.IntegerField(default=100)
    weight = models.FloatField(default=100)
    player_type = models.IntegerField(choices=PLAYER_TYPES, default=0)
    about= models.CharField(max_length=500, null=True)
    def __str__(self):
        return self.name

class Management(models.Model):
    date_added = models.DateField(default=timezone.now)
    name = models.CharField(max_length=150)
    picture = models.FileField(max_length=150, upload_to="management", blank=True, null=True)
    dob = models.DateField(blank=True, null=True, help_text="Date of Birth")
    nationality = models.CharField(max_length=50, default="Indian")
    height = models.IntegerField(default=100)
    weight = models.FloatField(default=100)
    manager_type = models.IntegerField(choices=MANAGER_TYPES, default=0)

    def __str__(self):
        return self.name

class Team(models.Model):
    date_added = models.DateField(default=timezone.now)
    competition = models.ForeignKey(Competition, blank=True, null=True, related_name='competition_admin')
    name = models.CharField(max_length=50)
    logo = models.FileField(upload_to="teams/logos", blank=True, null=True, max_length=150)
    state = models.CharField(max_length=35)
    players=models.ManyToManyField(Player)
    managers=models.ManyToManyField(Management)
    locations=models.CharField(max_length=35, null=True)
    about= models.CharField(max_length=500, null=True)
    email=models.EmailField(null=True)
    contact_no = PhoneNumberField(null=True)

    def __str__(self):
        return self.name


class Match(models.Model):
    date_added = models.DateField(default=timezone.now)
    date_of_match = models.DateField()
    season = models.ForeignKey(Season, default=None)
    preview = RichTextUploadingField(blank=True, null=True)
    report = RichTextUploadingField(blank=True, null=True)
    home_team = models.ForeignKey(Team, related_name="home")
    home_players_playing = models.ManyToManyField(Player, blank=True, related_name="home_players")
    home_players_substitutes = models.ManyToManyField(Player, blank=True, related_name="home_players_subs")
    away_team = models.ForeignKey(Team, related_name="away")
    away_players_playing = models.ManyToManyField(Player, blank=True, related_name="away_players")
    away_players_substitutes = models.ManyToManyField(Player, blank=True, related_name="away_players_subs")
    home_score = models.IntegerField(default=0)
    away_score = models.IntegerField(default=0)
    match_location = models.ForeignKey(Venue)
    published = models.BooleanField(default=False, choices=(
        (True, "Yes"),
        (False, "No"),
    ))

    def __str__(self):
        #return "Match between " + self.home_team.name + " and " + self.away_team.name + " on " + str(self.date_of_match) + " at " + self.match_location
        return "Match between " + self.home_team.name + " and " + self.away_team.name
    def match_description(self):
        #return "Match between " + self.home_team.name + " and " + self.away_team.name + " on " + str(self.date_of_match) + " at " + self.match_location
        return "Match between " + self.home_team.name + " and " + self.away_team.name

    def getHome(self):
        return self.home_team.name

    def getAway(self):
        return self.away_team.name

class TopPlayer(models.Model):
    date_added = models.DateField(default=timezone.now)
    player = models.ForeignKey(Player)

    def __str__(self):
        return self.player.name

class PointTable(models.Model):
    date_added = models.DateField(default=timezone.now)
    competition = models.ForeignKey(Competition)

    def __str__(self):
        return self.competition.name

class TeamPoint(models.Model):
    competition = models.ForeignKey(PointTable)
    team = models.ForeignKey(Team)
    points = models.IntegerField(default=0)
    wins = models.IntegerField(default=0)
    lose = models.IntegerField(default=0)
    draws = models.IntegerField(default=0)
    goal_for=models.IntegerField(default=0)
    goal_against=models.IntegerField(default=0)
    goal_diff=models.IntegerField(default=0)
