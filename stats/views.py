from django.shortcuts import render
from datetime import datetime, timedelta
from .models import Team, Match, Competition, Player, Season, PointTable, TeamPoint

teams = Team.objects.all().order_by("-date_added")


def index(request):
    return render(request, "index.html", {

    })

def clIndex(request):
    return render(request, "index.html", {

    })

def cupPage(request, cup_id, cup_name):
    return render(request, "index.html", {

    })


def playerPage(request, player_id, player_slug):
    player_id = int(player_id)
    player = Player.objects.get(pk=player_id)
    print ("player=", player);
    return render(request, "stats/player.html", {
        "player":player,
    })

def playerIndex(request):
    return render(request, "players.html", {

    })

def matchPage(request, match_id, match_slug):
    match=Match.objects.get(pk=match_id);
    for each in match.home_players_playing.all():
        print each.picture.url
    return render(request, "stats/match.html", {
        'match': match,
    })

def teamIndex(request):
    competitions = Competition.objects.all()
    teams = Team.objects.all()
    for each in competitions:
        each.team_set.all()

    return render(request, "stats/teams.html", {
    })

def teamPage(request, team_id, team_slug):
    team_id = int(team_id)
    team = Team.objects.get(pk=team_id)
    matches=Match.objects.filter(home_team=team_id)


    # for each in matches:
    #     print("teams in all matches=", each.home_team, each.away_team)
    #     if team_id==each.home_team or team_id==each.away_team:
    #         print("match=", each.name)

    startdate = datetime.today()
    # prevdate = startdate + timedelta(days=-365*100)
    # enddate = startdate + timedelta(days=10)
    # print("startdate", startdate," prevdate", prevdate,  " enddate", enddate)


    #objects.filter(date__range=["2011-01-01", "2011-01-31"])

    # fixtures1=[]
    # fixtures2=[]

    # results1=[]
    # results2=[]

    try:
        fixtures1=(Match.objects.filter(home_team=team_id).filter(date_of_match__gte=startdate))
    except:
        fixtures1 = None
    try:
        fixtures2 = (Match.objects.filter(away_team=team_id).filter(date_of_match__gte=startdate))
    except:
        fixtures2 = None

    try:
        results1.append(Match.objects.filter(home_team=team_id).filter(date_of_match__lt=startdate))
    except:
        results1 = None

    try:
        results2=(Match.objects.filter(away_team=team_id).filter(date_of_match__lt=startdate))

    except:
        results2 = None

    # print("fixtures1=", fixtures1, " results1=",  results1)
    # print("fixtures2=", fixtures2, " results2=",  results2)
    # results1.extend(results2)
    # fixtures1.extend(fixtures2)

    # print("fixtures1=", fixtures1, " results1=",  results1)
    # print("fixtures2=", fixtures2, " results2=",  results2)

    # for each in fixtures1:
    #     print "matchsfkjdbskfjsd=", each.date_added
        #each.home_team, each.away_team, each.date_of_match, each.season.competiton, each.match_location
    # for each in fixtures2:
    #     print each.home_team, each.away_team, each.date_of_match, each.season.competiton, each.match_location

    # for each in results1:
    #     print each.home_team, each.away_team, each.date_of_match, each.season.competiton, each.match_location

    # for each in results2:
    #     print each.home_team, each.away_team, each.date_of_match, each.season.competiton, each.match_location





    return render(request, "stats/team.html", {
        "team":team,
        "fixtures1":fixtures1,
        "results1":results1,
        "fixtures2":fixtures2,
        "results2":results2,

    })

def teamRoster(request, team_id, team_slug):
    team_id = int(team_id)
    team = Team.objects.get(pk=team_id)

    return render(request, "stats/team-roster.html", {
     "team":team,
    })

def teamSchedule(request, team_id, team_slug):
    team_id = int(team_id)
    team = Team.objects.get(pk=team_id)

    print "HERE", team, team_id
    matches=Match.objects.all()
    for each in matches:
        print each.home_team, each.away_team

    try:
        home_matches = Match.objects.filter(home_team=team_id)
    except:
        home_matches=None
    try:
        away_matches = Match.objects.filter(away_team=team_id)
    except:
        away_matches=None


    #home_matches = Match.objects.get(home_team=2)
    print "HERE, home matches=", home_matches

    #away_matches = Match.objects.get(away_team=2)
    print "HERE,  away_matches=", away_matches


    for each in home_matches:
        print each.home_team, each.away_team, each.date_of_match, each.season.competiton, each.match_location

    return render(request, "stats/team-schedule.html", {
     "team":team,
    "away_matches":away_matches, "home_matches":home_matches,
    })


def leaguePage(request, league_id, league_slug):

    league_id = int(league_id)
    league = Competition.objects.get(pk=league_id)
    seasons = Season.objects.filter(competiton=league)
    print "season", seasons

    startdate = datetime.today()

    results=Match.objects.filter(season__in=seasons).filter(date_of_match__lt=startdate)
    fixtures=Match.objects.filter(season__in=seasons).filter(date_of_match__gte=startdate).order_by("-date_of_match")
    print "results", results, "fixtures", fixtures

    teams = Team.objects.filter(competition=league_id);

    pointTable = PointTable.objects.filter(competition=league_id)
    teamPoint = TeamPoint.objects.filter(competition__in=pointTable)

    return render(request, "stats/league.html", {
        "league":league,
        "results":results,
        "fixtures":fixtures,
        "teams": teams,
        "teamPoint": teamPoint

    })
