from .models import Competition, Team

def dropdown_menu_info(context):
    teams = Team.objects.all()
    competitions = Competition.objects.all()

    return {'teams':teams, 'competitions':competitions} 
