# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-08 06:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stats', '0002_auto_20170608_1137'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='away_players',
            field=models.ManyToManyField(blank=True, related_name='away_players', to='stats.Player'),
        ),
        migrations.AlterField(
            model_name='match',
            name='home_players',
            field=models.ManyToManyField(blank=True, related_name='home_players', to='stats.Player'),
        ),
    ]
