# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Competition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=datetime.datetime(2017, 6, 7, 12, 41, 48, 599000, tzinfo=utc))),
                ('name', models.CharField(max_length=150)),
                ('league_format', models.IntegerField(choices=[(1, 'League'), (2, 'League + Knockout'), (3, 'Group-Wise League + Knockout'), (4, 'Group-Wise League + Leauge'), (5, 'Group Wise League + Group Wise League + Knockouts'), (6, 'League + Group-wise Leauge'), (7, 'Group-wise League + League + Knockout')], default=0)),
                ('logo', models.FileField(max_length=150, upload_to='competitions/logos/')),
            ],
        ),
        migrations.CreateModel(
            name='Management',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=datetime.datetime(2017, 6, 7, 12, 41, 48, 599000, tzinfo=utc))),
                ('name', models.CharField(max_length=150)),
                ('picture', models.FileField(blank=True, max_length=150, null=True, upload_to='management')),
                ('dob', models.DateField(blank=True, help_text='Date of Birth', null=True)),
                ('nationality', models.CharField(default='Indian', max_length=50)),
                ('height', models.IntegerField(default=100)),
                ('weight', models.FloatField(default=100)),
                ('manager_type', models.IntegerField(choices=[(1, 'Technical Director'), (2, 'Head Coach'), (3, 'Asst. Coach'), (4, 'GK Coach'), (5, 'Manager')], default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=datetime.datetime(2017, 6, 7, 12, 41, 48, 599000, tzinfo=utc))),
                ('date_of_match', models.DateField()),
                ('home_score', models.IntegerField(default=0)),
                ('away_score', models.IntegerField(default=0)),
                ('published', models.BooleanField(choices=[(True, 'Yes'), (False, 'No')], default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=datetime.datetime(2017, 6, 7, 12, 41, 48, 599000, tzinfo=utc))),
                ('name', models.CharField(max_length=150)),
                ('picture', models.FileField(blank=True, max_length=150, null=True, upload_to='players')),
                ('dob', models.DateField(blank=True, help_text='Date of Birth', null=True)),
                ('nationality', models.CharField(default='Indian', max_length=50)),
                ('height', models.IntegerField(default=100)),
                ('weight', models.FloatField(default=100)),
                ('player_type', models.IntegerField(choices=[(1, 'Centre-back'), (2, 'Sweeper'), (3, 'Full-back'), (4, 'Wing-back'), (5, 'Centre midfield'), (6, 'Defensive midfield'), (7, 'Attacking midfield'), (8, 'Wide midfield'), (9, 'Centre forward'), (10, 'Second striker'), (11, 'Winger'), (12, 'Goalkeeper'), (13, 'Defensive'), (14, 'Midfield'), (15, 'Attacking')], default=0)),
                ('about', models.CharField(max_length=500, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Season',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=datetime.datetime(2017, 6, 7, 12, 41, 48, 599000, tzinfo=utc))),
                ('name', models.CharField(max_length=50)),
                ('competiton', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stats.Competition')),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=datetime.datetime(2017, 6, 7, 12, 41, 48, 599000, tzinfo=utc))),
                ('name', models.CharField(max_length=50)),
                ('logo', models.FileField(blank=True, max_length=150, null=True, upload_to='teams/logos')),
                ('state', models.CharField(max_length=35)),
                ('locations', models.CharField(max_length=35, null=True)),
                ('about', models.CharField(max_length=500, null=True)),
                ('email', models.EmailField(max_length=254, null=True)),
                ('contact_no', phonenumber_field.modelfields.PhoneNumberField(max_length=128, null=True)),
                ('competition', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='stats.Competition')),
                ('managers', models.ManyToManyField(to='stats.Management')),
                ('players', models.ManyToManyField(to='stats.Player')),
            ],
        ),
        migrations.CreateModel(
            name='TopPlayer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=datetime.datetime(2017, 6, 7, 12, 41, 48, 599000, tzinfo=utc))),
                ('player', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stats.Player')),
            ],
        ),
        migrations.CreateModel(
            name='Venue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=datetime.datetime(2017, 6, 7, 12, 41, 48, 599000, tzinfo=utc))),
                ('name', models.CharField(max_length=150)),
                ('address', models.TextField(blank=True, null=True)),
                ('city', models.CharField(blank=True, max_length=50, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='match',
            name='away_players',
            field=models.ManyToManyField(blank=True, null=True, related_name='away_players', to='stats.Player'),
        ),
        migrations.AddField(
            model_name='match',
            name='away_team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='away', to='stats.Team'),
        ),
        migrations.AddField(
            model_name='match',
            name='home_players',
            field=models.ManyToManyField(blank=True, null=True, related_name='home_players', to='stats.Player'),
        ),
        migrations.AddField(
            model_name='match',
            name='home_team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='home', to='stats.Team'),
        ),
        migrations.AddField(
            model_name='match',
            name='match_location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stats.Venue'),
        ),
        migrations.AddField(
            model_name='match',
            name='season',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='stats.Season'),
        ),
    ]
