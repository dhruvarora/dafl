# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-08 08:08
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('stats', '0003_auto_20170608_1138'),
    ]

    operations = [
        migrations.CreateModel(
            name='PointTable',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=django.utils.timezone.now)),
                ('competition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stats.Competition')),
            ],
        ),
        migrations.CreateModel(
            name='TeamPoint',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('points', models.IntegerField(default=0)),
                ('wins', models.IntegerField(default=0)),
                ('lose', models.IntegerField(default=0)),
                ('draws', models.IntegerField(default=0)),
                ('competition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stats.PointTable')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='stats.Team')),
            ],
        ),
    ]
