**_ Make sure you have done "pip install -r requirements.txt" to install required modules. Always do pip install inside a virtualenv and never ever add virtualenv to the repo _**

Always make sure to update the requirements.txt whenever you install a new library and update the installed appas and urls.py in the readme.md

###### Your installed apps in settings should look like:
```
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'ckeditor',
    'ckeditor_uploader',
    'website',
    'articles',
    'stats',
    'poll',
    'photologue',
    'sortedm2m',
]
```


**and the main urls.py as:-**
```
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include("website.urls", namespace="website")),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^articles/', include("articles.urls", namespace="articles")),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^poll/', include('poll.urls', namespace="poll")),
    url(r'^football-center/', include("stats.urls", namespace="stats")),
    url(r'^photologue/', include('photologue.urls', namespace='photologue')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```
