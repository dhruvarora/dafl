# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.shortcuts import render
from .models import Article

def index(request, page):

    page=int(page)
    print("page=", page)
    print("hereherehereherehereherehereherehereherehereherehereherehereherehere")
    

    latest_articles_list = Article.objects.filter(published=True).order_by("-date_added")
    print("page=", page)

    paginator = Paginator(latest_articles_list, 4)

    try:
        latest_articles = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        latest_articles = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        latest_articles = paginator.page(paginator.num_pages)


    return render(request, "articles/index.html", {
    "articles":latest_articles,
    })


def article(request, id, article_slug):
    article = Article.objects.get(pk=id)
    latest_articles = Article.objects.filter(published=True).order_by("-date_added")[:3]

    return render(request, "articles/article.html", {
    "article":article,
    "articles":latest_articles,
    })
