# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-07 12:41

from __future__ import unicode_literals
import ckeditor_uploader.fields
import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=datetime.datetime(2017, 6, 7, 12, 41, 48, 450000, tzinfo=utc))),
                ('published', models.BooleanField(choices=[(True, 'Yes'), (False, 'No')], default=True)),
                ('featured', models.BooleanField(choices=[(True, 'Yes'), (False, 'No')], default=True)),
                ('title', models.CharField(max_length=150)),
                ('author_name', models.CharField(max_length=150)),
                ('intro_text', models.TextField(blank=True, null=True)),
                ('image', models.FileField(blank=True, max_length=200, null=True, upload_to='articles/intro-images')),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True)),
                ('tags', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateField(default=datetime.datetime(2017, 6, 7, 12, 41, 48, 450000, tzinfo=utc))),
                ('published', models.BooleanField(choices=[(True, 'Yes'), (False, 'No')], default=True)),
                ('title', models.CharField(max_length=150)),
                ('image', models.FileField(max_length=200, upload_to='articles/videos')),
                ('youtube_link', models.CharField(max_length=300)),
            ],
        ),
    ]
