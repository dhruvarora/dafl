# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField

YESNO_CHOICES = (
    (True, "Yes"),
    (False, "No"),
)

class Article(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    published = models.BooleanField(choices=YESNO_CHOICES, default=True)
    featured = models.BooleanField(choices=YESNO_CHOICES, default=True)
    title = models.CharField(max_length=150)
    author_name = models.CharField(max_length=150)
    intro_text = models.TextField(blank=True, null=True)
    image = models.FileField(upload_to="articles/intro-images", blank=True, null=True, max_length=200)
    content = RichTextUploadingField(blank=True, null=True)
    tags = models.CharField(max_length=250)

    def __str__(self):
        return self.title


class Video(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    published = models.BooleanField(choices=YESNO_CHOICES, default=True)
    title = models.CharField(max_length=150)
    image = models.FileField(max_length=200, upload_to="articles/videos")
    youtube_link = models.CharField(max_length=300)

    def __str__(self):
        return self.title
