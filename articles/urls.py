from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<page>\d+)/$', views.index, name="index"),
    url(r'^(?P<id>\d+)/(?P<article_slug>.+)/$', views.article, name="article"),
]
