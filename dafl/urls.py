from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include("website.urls", namespace="website")),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^articles/', include("articles.urls", namespace="articles")),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^poll/', include('poll.urls', namespace="poll")),
    url(r'^football-center/', include("stats.urls", namespace="stats")),
    url(r'^photologue/', include('photologue.urls', namespace='photologue')),
    url(r'^registration/', include("registration.urls", namespace="registration")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
