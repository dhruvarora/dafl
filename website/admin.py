from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from django.db import models

from ckeditor_uploader.widgets import CKEditorUploadingWidget as CKEditorWidget
from .models import Query

class FlatPageCustom(FlatPageAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget}
    }

class QueryAdmin(admin.ModelAdmin):
    list_display = ["name", "email", "phone", "message", "date"]

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageCustom)
admin.site.register(Query, QueryAdmin)
