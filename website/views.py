# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from articles.models import Article, Video
from stats.models import TopPlayer
from .models import Query
from django.core.mail import send_mail

def index(request):
    latest_articles = Article.objects.filter(published=True, featured=True).order_by("-date_added")[:4]
    latest_videos = Video.objects.filter(published=True).order_by("-date_added")[:4]
    top_players = TopPlayer.objects.all()[:4]
    print top_players

    return render(request, "index.html", {
    "latest_articles":latest_articles, "latest_videos":latest_videos,
    "top_players": top_players,
    })

def contact(request):
    if request.method == "POST":
        name = request.POST.get("name")
        email = request.POST.get("email")
        phone = request.POST.get("phone")
        message = request.POST.get("message")
        q = Query(name=name, email=email, phone=phone, message=message)
        q.save()
        send_mail("You have received a query on DAFL Website",
        "Hi, You have received a query on the DAFL Wesbsite. Please check the admin website on dafl.co/admin to see new querys.",
        "query@dafl.co",
        ["babuabiswas2012@gmail.com", "aasheesh@dafl.co"])

        return render(request, "contact.html", {
        "status":"done"
        })
    else:
        return render(request, "contact.html", {})

def feedback(request):
    if request.method == "POST":
        name = request.POST.get("name")
        email = request.POST.get("email")
        phone = request.POST.get("phone")
        message = request.POST.get("message")
        q = Query(name=name, email=email, phone=phone, message=message)
        q.save()
        send_mail("You have received a feedback on DAFL Website",
        "Hi, You have received a feedback on the DAFL Wesbsite. Please check the admin website on dafl.co/admin to see new querys.",
        "query@dafl.co",
        ["babuabiswas2012@gmail.com", "aasheesh@dafl.co"])

        return render(request, "feedback.html", {
        "status":"done"
        })
    else:
        return render(request, "contact.html", {})
