# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.flatpages.models import FlatPage

class Query(models.Model):
    date = models.DateField(default=timezone.now)
    name = models.CharField(max_length=150)
    email = models.EmailField(max_length=150)
    phone = models.BigIntegerField()
    message = models.TextField()

    def __str__(self):
        return "Query by " + self.name + " on " + str(self.date)

class FlatPageExtended(FlatPage):
    pdf = models.FileField(upload_to="flat-pages/pdfs", max_length=150, blank=True, null=True)
