# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from stats.models import Competition


TEAMTYPES_CHOICES = (
    (1, "Individual"),
    (2, "Friends"),
    (3, "Team"),
)

class Team(models.Model):
    date_added = models.DateField(default=timezone.now)
    name = models.CharField(max_length=150)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    dob = models.DateField()
    email = models.EmailField(max_length=150)
    phone = models.BigIntegerField()
    dob_proof = models.FileField(max_length=150, upload_to="new-teams/dob-proofs/")
    fees = models.FloatField(default=500.00)
    team_type = models.IntegerField(choices=TEAMTYPES_CHOICES, default=0)
    alternate_phone = models.BigIntegerField(null=True)
    competition = models.ForeignKey(Competition, blank=True, null=True, related_name='competition_user')



    def __str__(self):
        return self.name

class Transaction(models.Model):
    date_added = models.DateField(default=timezone.now)
    team = models.ForeignKey(Team)
    txn_amount = models.FloatField(default=500)
    txn_id = models.CharField(max_length=75)
    txn_status = models.CharField(max_length=75)

class Player(models.Model):
    team = models.ForeignKey(Team)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    dob = models.DateField()
    dob_proof = models.FileField(max_length=200, upload_to="registration-teams/players/dob-proofs/")

    def __str__(self):
        return self.first_name + self.last_name
