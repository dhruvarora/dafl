# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Team, Transaction, Player

class TransactionInline(admin.TabularInline):
    model = Transaction
    extra = 0

class PlayerInline(admin.StackedInline):
    model = Player
    extra = 3

class TeamAdmin(admin.ModelAdmin):
    list_display = ["name", "first_name", "last_name", "date_added", "email", "phone", "team_type"]
    list_filter = ["team_type"]
    search_fields = ["name", "first_name", "last_name", "email"]
    inlines = [TransactionInline, PlayerInline]

admin.site.register(Team, TeamAdmin)
admin.site.register(Transaction)
admin.site.register(Player)
