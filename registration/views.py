# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from stats.models import Competition

from .models import Team, Player
import datetime

# from .forms import TeamForm
TEAMTYPES= {
    '1': "individual",
    '2': "friends",
    '3': "team",
}

def index(request):
    category = Competition.objects.all();
    num=31 #number of players for registeration + 1
    if request.method == "POST":
        team_type = request.POST.get("type")
        team_name = request.POST.get("team_name")
        first_name = request.POST.get("first_name1")
        last_name = request.POST.get("last_name1")

        x=str(request.POST.get('DOB1'))
        x=x.split("-")
        DOB=datetime.date(int(x[0]),int(x[1]),int(x[2]))

        email = request.POST.get("email")
        phone = request.POST.get("phone")
        alternate_phone = request.POST.get("alternate_phone")
        try:
            dob_proof = request.FILES["dob_proof1"]
        except:
            dob_proof = None
        comp_value=request.POST.get("category")
        print("category=", comp_value)

        competition = Competition.objects.filter(name=comp_value)[:1].get()

        t = Team(name=team_name,first_name=first_name, last_name=last_name,  dob=DOB,
                  email=email,alternate_phone=alternate_phone,  competition=competition,  phone=phone, dob_proof=dob_proof, team_type=int(TEAMTYPES.keys()[TEAMTYPES.values().index(team_type)]) )
        t.save()

        team= Team.objects.filter(name=team_name).order_by("-date_added")[:1].get()

        for i in xrange(1, num):
            first_name = request.POST.get("first_name"+str(i))
            last_name = request.POST.get("last_name"+str(i))

            x=str(request.POST.get("DOB"+str(i)))
            x=x.split("-")
            DOB=datetime.date(int(x[0]),int(x[1]),int(x[2]))

            #dob_proof = request.FILES["dob_proof"+str(i)]
            try:
                dob_proof = request.FILES["dob_proof"+str(i)]
            except:
                dob_proof = None

            p=Player(team = team, first_name=first_name, last_name = last_name, dob=DOB, dob_proof = dob_proof)
            p.save()


        return render(request, "registration/index.html", {
        "status":"done",
        "category": category,
        "message":"Registration Successful!!!",
        'range3': xrange(1,num),
        })

    else:
        return render(request, "registration/index.html", {
        "status":"",
        "category": category,
        "message":"",
        'range3': xrange(1,num),
        })
